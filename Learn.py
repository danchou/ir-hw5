
import mlpy
import numpy as np

DEBUG = 1

def loadTrainingData(fname):
    data = np.loadtxt(fname, delimiter=',', dtype=None, usecols=(2,3,4,5,6,7))
    return data

def loadTestFeatures(fname):
    docno = []
    buf = []
    with open(fname) as f:
        for line in f:
            lst = line.strip().split(',')
            docno.append((lst[0], lst[1]))
            buf.append([float(e) for e in lst[2:]])
    return buf, docno

def learn(data, C=1):
    lr = mlpy.LibLinear(solver_type='l1r_lr')
    lr.learn(data[:,0:-1], data[:,-1])
    buf, docno = loadTestFeatures('data/test_all_features_2')

    data = np.array(buf)
    scores = lr.pred_probability(data)[:,0]

    dic = {}
    for i, pair in enumerate(docno):
        try:
            dic[pair[0]].append((pair[1], scores[i]))
        except:
            dic[pair[0]] = [(pair[1], scores[i])]
    
    for qid, value in dic.iteritems():
        value.sort(key=lambda x: x[1], reverse=True)
        i = 0
        while i<len(value) and i<100:
            print qid, 'Q0', value[i][0], i+1, value[i][1], 'Exp'
            i += 1

def test():
    learn(loadTrainingData('data/features'))

    
def main():
    pass

if __name__ == '__main__':
    if DEBUG: test()
    else: main()
